package me.kolade.quizattack.activity;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import java.util.HashMap;
import java.util.Map;

import me.kolade.quizattack.R;
import me.kolade.quizattack.handler.WebManager;
import me.kolade.quizattack.utility.ActivityUtility;
import me.kolade.quizattack.utility.LayoutUtility;

public class RegisterActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0){
            ActivityUtility.startActivity(this, WelcomeActivity.class);
        }

        return false;
    }

    public void onRegister(View view) {
        //Enables layout
        LayoutUtility.toggleActivityLayout((ConstraintLayout) findViewById(R.id.layout), false);

        //Show progress bar overlay
        LayoutUtility.setViewVisbility(findViewById(R.id.relativeLayout), true);

        //Get email and password from text fields
        EditText txtEmail = findViewById(R.id.txtEmail);
        EditText txtPassword = findViewById(R.id.txtPassword);
        EditText txtDisplayName = findViewById(R.id.txtDisplayName);
        String email = txtEmail.getText().toString();
        String password = txtPassword.getText().toString();
        String displayName = txtDisplayName.getText().toString();

        Map<String, Object> params = new HashMap<>();
        params.put("email", email);
        params.put("password", password);
        params.put("displayName", displayName);
        params.put("profileImage", "");

        WebManager.request(WebManager.API_URL + "api/account", WebManager.RequestType.POST, params, new WebManager.RequestListener() {
            @Override
            public void onResponse(WebManager.ResponseObject response) {
                if (response.getBoolean("success")) {
                    ActivityUtility.startActivity(RegisterActivity.this, LoginActivity.class);
                    finish();
                    return;
                }

                //Enables layout
                LayoutUtility.toggleActivityLayout((ConstraintLayout) findViewById(R.id.layout), true);

                //Show progress bar overlay
                LayoutUtility.setViewVisbility(findViewById(R.id.relativeLayout), false);

                //Show error message
                ActivityUtility.toast(RegisterActivity.this, response.getString("message"), 45);
            }
        });
    }
}
