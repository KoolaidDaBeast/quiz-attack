package me.kolade.quizattack.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.json.JSONObject;

import me.kolade.quizattack.R;
import me.kolade.quizattack.fragment.HomeFragment;
import me.kolade.quizattack.handler.SocketManager;
import me.kolade.quizattack.handler.User;
import me.kolade.quizattack.handler.WebManager;
import me.kolade.quizattack.listeners.ActivityListeners;
import me.kolade.quizattack.listeners.SocketService;
import me.kolade.quizattack.utility.ActivityUtility;
import me.kolade.quizattack.utility.LayoutUtility;
import me.kolade.quizattack.utility.NotificationUtility;
import me.kolade.quizattack.wrapper.SharedPreferencesWrapper;

public class MainActivity extends AppCompatActivity {

    private SharedPreferencesWrapper sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Create notification channel
        NotificationUtility.createNotificationChannel(this);

        //Start service
        Intent intent = new Intent(this, SocketService.class);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForegroundService(intent);
        }

        sp = new SharedPreferencesWrapper(this);

        //Set navbar selected item
        BottomNavigationView navBar = findViewById(R.id.navBottom);
        navBar.setSelectedItemId(R.id.menuItemHome);

        //Start listener for the navbar
        ActivityListeners.bottomNavBarSelectListener(this);

        //Listen for navbar clicks (HOME FRAGMENT)
        BottomNavigationView homeNavMenu = findViewById(R.id.navHome);
        homeNavMenu.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.menuItemSettings:
                        ActivityUtility.startActivity(MainActivity.this, SettingActivity.class);
                        finish();
                        break;
                }

                return true;
            }
        });

        //Download extra stuff if missing
        if (!sp.hasKey("topics")) {
            WebManager.requestGet(WebManager.API_URL + "api/topics?key=" + User.getLocalApiKey(this), new WebManager.RequestListener() {
                @Override
                public void onResponse(WebManager.ResponseObject response) {
                    if (response.getBoolean("success")) {
                        sp.addValue("topics", response.getString("topics"));
                        NotificationUtility.sendNotification(MainActivity.this, "", "Topics have been successfully downloaded.");
                        return;
                    }
                }
            });
        }

        constructHomeFragment();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0){

        }

        return true;
    }

    private void constructHomeFragment() {
        //Show loading bar because we are loading data
        LayoutUtility.setViewVisbility(findViewById(R.id.relativeLayout), true);

        WebManager.requestGet(WebManager.API_URL + "api/account/details?key=" + User.getLocalApiKey(this) + "&id=" + User.getLocalUserId(this), new WebManager.RequestListener() {
            @Override
            public void onResponse(final WebManager.ResponseObject response) {
                //Hide loading bar
                LayoutUtility.setViewVisbility(findViewById(R.id.relativeLayout), false);

                //Save user data locally
                sp.addValue("data", response.getString("data"));

                //Perform this in main thread
                ActivityUtility.runSyncTask(new ActivityUtility.SyncTask() {
                    @Override
                    public void run() {
                        try {
                            JSONObject data = new JSONObject(sp.getString("data"));

                            //Construct page
                            TextView txtDisplayName = findViewById(R.id.lblDisplayName);
                            txtDisplayName.setText(data.getString("displayName"));

                            if (data.getString("profileImage").trim().length() > 1) {
                                sp.addValue("profileImage", data.getString("profileImage"));
                            }

                            if (sp.hasKey("profileImage")) {
                                ImageView imgProfile = findViewById(R.id.imgProfile);
                                imgProfile.setImageBitmap(LayoutUtility.decodeBase64(sp.getString("profileImage")));
                            }


                        }
                        catch (Exception e) {}
                    }
                });
                //END//

            }
        });
    }


}
