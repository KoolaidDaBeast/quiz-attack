package me.kolade.quizattack.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import me.kolade.quizattack.R;
import me.kolade.quizattack.adapter.QuestionItem;
import me.kolade.quizattack.handler.Quiz;
import me.kolade.quizattack.utility.ActivityUtility;
import me.kolade.quizattack.utility.QuizUtility;
import me.kolade.quizattack.wrapper.SharedPreferencesWrapper;

public class QuizGameActivity extends AppCompatActivity {

    private int MAX_SECONDS = 10, CURRENT_QUESTION = 0, SCORE = 0, TIME_LEFT = 0;
    private CountDownTimer previewTimer, questionTimer, resultTimer;
    private HashMap<Integer, Boolean> correctAnswers = new HashMap<>();
    private Quiz quiz;
    private boolean DISABLE_TOUCH = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_game);

        SharedPreferencesWrapper sp = new SharedPreferencesWrapper(this);
        final ProgressBar progressBar = findViewById(R.id.progressBar);
        final TextView lblTime = findViewById(R.id.txtTimeLeft);
        final TextView lblQuestion = findViewById(R.id.lblQuestion);
        final ImageView imgContent = findViewById(R.id.imgContent);
        imgContent.setImageBitmap(null);

        //Button options
        final TextView btnOption1 = findViewById(R.id.btnOption1);
        final TextView btnOption2 = findViewById(R.id.btnOption2);
        final TextView btnOption3 = findViewById(R.id.btnOption3);
        final TextView btnOption4 = findViewById(R.id.btnOption4);

        //Setup quiz progress bar timer
        progressBar.setMax(MAX_SECONDS);

        //Get quiz metadata
        final String quizId = getIntent().getStringExtra("quizId");
        final List<QuestionItem> questions = new ArrayList<QuestionItem>();

        //Asynchronously get quiz to avoid blocking in main UI thread
        ActivityUtility.runAsyncTask(new Runnable() {
            @Override
            public void run() {
                quiz = QuizUtility.getQuiz(QuizGameActivity.this, quizId);

                if (quiz == null) {
                    ActivityUtility.alertBox(QuizGameActivity.this, "ERROR: " + quizId);
                    return;
                }

                questions.addAll(quiz.getQuestions());
                previewTimer.start();
            }
        });


        //Timer that handles the time for each question
        //Add extra time so things can load in
        questionTimer = new CountDownTimer((MAX_SECONDS) * 1000 + 500, 1000) {

            @Override
            public void onTick(long millisUntilFinished) {
                //Don't do anything during the extra time for items to appear
                if (millisUntilFinished / 1000 >= MAX_SECONDS) {
                    TIME_LEFT = MAX_SECONDS;
                    return;
                }

                //If time left is zero just skip to finish
                if (millisUntilFinished == 0) {
                    finish();
                    return;
                }

                int currentProgess = (int) (MAX_SECONDS - (MAX_SECONDS - (millisUntilFinished / 1000)));
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    progressBar.setProgress(currentProgess, true);
                }

                lblTime.setText(String.valueOf((millisUntilFinished / 1000)));
                TIME_LEFT = (int) millisUntilFinished / 1000;
            }

            @Override
            public void onFinish() {
                //Show correct answer if user did not pick in time
                int correctViewId = getResources().getIdentifier("btnOption" + (quiz.getQuestions().get(CURRENT_QUESTION).getCorrectQuestion() + 1), "id", getPackageName());
                TextView correctView = findViewById(correctViewId);
                correctView.setBackgroundColor(Color.YELLOW);
                correctAnswers.put(CURRENT_QUESTION, false);
                resultTimer.start();
            }

        };

        //Start preview question timer
        previewTimer = new CountDownTimer((MAX_SECONDS / 3) * 1000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                //Set timer hud display
                lblTime.setText(String.valueOf(MAX_SECONDS));
                progressBar.setProgress(progressBar.getMax());

                //Set buttons
                String[] options = questions.get(CURRENT_QUESTION).getOptions();
                for (int i = 0; i < options.length; i++) {
                    int id = getResources().getIdentifier("btnOption" + (i + 1), "id", getPackageName());
                    TextView btnOption = findViewById(id);
                    btnOption.setText(options[i]);
                }

                //Set image if there is one
                BitmapDrawable bitmapDrawable = (BitmapDrawable) imgContent.getDrawable();
                if (questions.get(CURRENT_QUESTION).getQuestionImage() != null && bitmapDrawable.getBitmap() == null) {
                    imgContent.setImageBitmap(questions.get(CURRENT_QUESTION).getQuestionImage());
                }

                //Set question
                lblQuestion.setText(questions.get(CURRENT_QUESTION).getQuestion());
            }

            @Override
            public void onFinish() {
                btnOption1.setVisibility(View.VISIBLE);
                btnOption2.setVisibility(View.VISIBLE);
                btnOption3.setVisibility(View.VISIBLE);
                btnOption4.setVisibility(View.VISIBLE);
                questionTimer.start();
            }
        };

        //Start this timer after answer has been selected
        resultTimer = new CountDownTimer((MAX_SECONDS / 3) * 1000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) { }

            @Override
            public void onFinish() {

                ActivityUtility.showUnCancelableDialog(QuizGameActivity.this, "Continue?", new String[]{"YES", "NO"}, new ActivityUtility.DialogListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int position) {
                        //Show next question
                        if (position == 0) {
                            CURRENT_QUESTION++;
                            imgContent.setImageBitmap(null);
                            btnOption1.setVisibility(View.GONE);
                            btnOption2.setVisibility(View.GONE);
                            btnOption3.setVisibility(View.GONE);
                            btnOption4.setVisibility(View.GONE);
                            btnOption1.setBackgroundColor(Color.WHITE);
                            btnOption2.setBackgroundColor(Color.WHITE);
                            btnOption3.setBackgroundColor(Color.WHITE);
                            btnOption4.setBackgroundColor(Color.WHITE);
                            previewTimer.start();
                            DISABLE_TOUCH = false;
                            return;
                        }

                        //User doesn't want to continue quiz so force quit
                        ActivityUtility.startActivity(QuizGameActivity.this, TopicActivity.class);
                    }
                });

            }
        };
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev){
        if (DISABLE_TOUCH){ return true; }
        return super.dispatchTouchEvent(ev);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            //Confirm if user wants to leave
            ActivityUtility.showUnCancelableDialog(this, "Are you sure you want to leave?", new String[]{"Yes", "No"}, new ActivityUtility.DialogListener() {
                @Override
                public void onClick(DialogInterface dialog, int position) {
                    if (position == 0) {
                        ActivityUtility.startActivity(QuizGameActivity.this, TopicActivity.class);
                    }
                }
            });
        }
        return false;
    }

    public void onClick(View view) {
        DISABLE_TOUCH = true;
        questionTimer.cancel();

        TextView lblScore = findViewById(R.id.lblScore);
        TextView txtSelectedOption = (TextView) view;

        int selectedOption = 0;
        if (view.getId() == R.id.btnOption1) { selectedOption = 0; }
        else if (view.getId() == R.id.btnOption2) { selectedOption = 1; }
        else if (view.getId() == R.id.btnOption3) { selectedOption = 2; }
        else if (view.getId() == R.id.btnOption4) { selectedOption = 3; }

        if (TIME_LEFT == 0) { TIME_LEFT = 1; }

        //Check answer
        if (selectedOption == quiz.getQuestions().get(CURRENT_QUESTION).getCorrectQuestion()) {
            txtSelectedOption.setBackgroundColor(Color.GREEN);
            correctAnswers.put(CURRENT_QUESTION, true);
            SCORE += TIME_LEFT * 1.5;
            lblScore.setText("Score: " + SCORE);
        }
        else {
            txtSelectedOption.setBackgroundColor(Color.RED);

            int correctViewId = getResources().getIdentifier("btnOption" + (quiz.getQuestions().get(CURRENT_QUESTION).getCorrectQuestion() + 1), "id", getPackageName());
            TextView correctView = findViewById(correctViewId);
            correctView.setBackgroundColor(Color.GREEN);
            correctAnswers.put(CURRENT_QUESTION, false);
        }

        //Quiz is over
        if (CURRENT_QUESTION + 1 >= quiz.getQuestions().size()) {
            Intent intent = new Intent();
            intent.putExtra("score", SCORE);
            intent.putExtra("results", correctAnswers);
            ActivityUtility.startActivity(this, QuizResultsActivity.class, intent);
            return;
        }

        resultTimer.start();
    }

}

