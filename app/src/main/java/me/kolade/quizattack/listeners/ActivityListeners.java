package me.kolade.quizattack.listeners;

import android.app.Activity;
import android.content.Context;
import android.view.MenuItem;

import androidx.annotation.NonNull;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import me.kolade.quizattack.R;
import me.kolade.quizattack.activity.FeedActivity;
import me.kolade.quizattack.activity.FriendActivity;
import me.kolade.quizattack.activity.MainActivity;
import me.kolade.quizattack.activity.QuizListActivity;
import me.kolade.quizattack.activity.TopicActivity;
import me.kolade.quizattack.utility.ActivityUtility;

public class ActivityListeners {

    public ActivityListeners() {

    }

    public static void bottomNavBarSelectListener(final Context ctx) {
        final Activity activity = (Activity) ctx;
        //Set default selected menu item (TOP NAV BAR)
        final BottomNavigationView navMenu = activity.findViewById(R.id.navBottom);

        //Listen for bottom navbar clicks (HOME FRAGMENT)
        navMenu.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.menuItemFeed:
                        if (activity instanceof FeedActivity) { return false; }
                        ActivityUtility.startActivity(ctx, FeedActivity.class);
                        activity.finish();
                        break;

                    case R.id.menuItemHome:
                        if (activity instanceof MainActivity) { return false; }
                        ActivityUtility.startActivity(ctx, MainActivity.class);
                        activity.finish();
                        break;

                    case R.id.menuItemFriends:
                        if (activity instanceof FriendActivity) { return false; }
                        ActivityUtility.startActivity(ctx, FriendActivity.class);
                        activity.finish();
                        break;

                    case R.id.menuItemQuiz:
                        if (activity instanceof TopicActivity) { return false; }
                        ActivityUtility.startActivity(ctx, TopicActivity.class);
                        activity.finish();
                        break;

                    case R.id.menuItemQuizList:
                        if (activity instanceof QuizListActivity) { return false; }
                        ActivityUtility.startActivity(ctx, QuizListActivity.class);
                        activity.finish();
                        break;
                }

                return false;
            }
        });
    }
}
