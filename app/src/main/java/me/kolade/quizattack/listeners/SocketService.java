package me.kolade.quizattack.listeners;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import org.json.JSONObject;

import me.kolade.quizattack.R;
import me.kolade.quizattack.activity.MainActivity;
import me.kolade.quizattack.handler.SocketManager;
import me.kolade.quizattack.handler.User;
import me.kolade.quizattack.utility.ActivityUtility;
import me.kolade.quizattack.utility.NotificationUtility;
import me.kolade.quizattack.wrapper.SharedPreferencesWrapper;

public class SocketService extends Service {


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        startForeground(1, NotificationUtility.buildServiceNotification(this));

        ActivityUtility.runAsyncTask(new Runnable() {
            @Override
            public void run() {
                SocketManager.setupSocket(SocketService.this);

                //Setup socket listeners
                SocketManager.userListener(SocketService.this);
            }
        });
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //Stop the service by itself if user has logged out (CRUDE METHOD)
        ActivityUtility.runAsyncTask(new Runnable() {
            @Override
            public void run() {
                SharedPreferencesWrapper sp = new SharedPreferencesWrapper(SocketService.this);

                boolean running = true;
                while(running) {
                    try {
                        if (!sp.hasKey("apiKey")) {
                            running = false;
                            break;
                        }

                        Thread.sleep(1000);
                    } catch (InterruptedException e) {}
                }

                stopSelf();
            }
        });

        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        //SocketManager.disconnectSocket();
    }

}
