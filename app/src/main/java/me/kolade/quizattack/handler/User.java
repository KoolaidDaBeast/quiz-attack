package me.kolade.quizattack.handler;

import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;

import me.kolade.quizattack.utility.ActivityUtility;
import me.kolade.quizattack.wrapper.SharedPreferencesWrapper;

public class User {

    public User() {

    }

    public static String getLocalDisplayName(Context ctx) {
        SharedPreferencesWrapper sp = new SharedPreferencesWrapper(ctx);
        try {
            JSONObject obj = new JSONObject(sp.getString("data"));

            return obj.getString("displayName");
        }
        catch (Exception e) {
            return "N/A";
        }
    }

    public static String getLocalApiKey(Context ctx) {
        SharedPreferencesWrapper sp = new SharedPreferencesWrapper(ctx);
        return sp.getString("apiKey");
    }

    public static String getLocalUserId(Context ctx) {
        SharedPreferencesWrapper sp = new SharedPreferencesWrapper(ctx);
        return sp.getString("userId");
    }

    public static JSONObject getRemoteUserDetails(final Context ctx, String id) {
        final JSONObject obj = new JSONObject();

        try {

            Thread thread = WebManager.syncRequestGet(WebManager.API_URL + "api/account/details?key=" + getLocalApiKey(ctx) + "&id=" + id, new WebManager.RequestListener() {
                @Override
                public void onResponse(WebManager.ResponseObject response) {
                    try {
                        JSONObject data = new JSONObject(response.getString("data"));
                        obj.put("displayName", data.getString("displayName"));
                        obj.put("image", data.getString("profileImage"));
                    }
                    catch (Exception e) { }
                }
            });

            thread.start();
            thread.join();

            return obj;
        }
        catch (Exception e) {
            e.printStackTrace();
            return obj;
        }
    }

}
