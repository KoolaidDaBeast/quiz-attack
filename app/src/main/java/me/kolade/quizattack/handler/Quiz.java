package me.kolade.quizattack.handler;

import android.graphics.Bitmap;
import android.widget.ListView;

import java.util.List;

import me.kolade.quizattack.adapter.QuestionItem;

public class Quiz {

    private String id, title, topicId;
    private List<QuestionItem> questions;
    private Bitmap icon;

    public Quiz(String id, String title, String topicId) {
        this.id = id;
        this.title = title;
        this.topicId = topicId;
    }

    public String getQuizId(){ return id; }

    public String getTitle(){ return title; }

    public String getTopicId(){ return topicId; }

    public List<QuestionItem> getQuestions() { return questions; }

    public Bitmap getIcon() { return icon; }

    public void setQuestions(List<QuestionItem> questions){
        this.questions = questions;
    }

    public void setIcon(Bitmap bmp) { this.icon = bmp; }

}
