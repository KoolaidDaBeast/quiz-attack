package me.kolade.quizattack.handler;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;

import me.kolade.quizattack.R;
import me.kolade.quizattack.utility.ActivityUtility;

public class InputDialog extends Dialog {

    private String title;

    public InputDialog(Context context, String title) {
        super(context);
        this.title = title;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_input);

        TextView lblTitle = findViewById(R.id.lblHeader);
        lblTitle.setText(title);
    }

    public void setSuccessListener(final DialogResult dr) {
        final EditText txtInput = findViewById(R.id.txtInput);
        Button btnOk = findViewById(R.id.btnSave);

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dr.onSuccess(txtInput.getText().toString());
                dismiss();
            }
        });
    }

    public interface DialogResult {
        void onSuccess(String result);
    }
}
