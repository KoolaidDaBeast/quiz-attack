package me.kolade.quizattack.adapter;

import java.util.List;

public class TopicItem {

    private String title;
    private List<QuizIconItem> icons;

    public TopicItem(String title, List<QuizIconItem> items) {
        this.title = title;
        this.icons = items;
    }

    public String getTitle() {
        return title;
    }

    public List<QuizIconItem> getIcons() {
        return icons;
    }
}
