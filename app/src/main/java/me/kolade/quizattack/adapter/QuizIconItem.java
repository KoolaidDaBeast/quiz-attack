package me.kolade.quizattack.adapter;

import android.graphics.Bitmap;

public class QuizIconItem {

    private String title, topic, id;
    private Bitmap bmp;

    public QuizIconItem(String id, String title, String topic, Bitmap bmp) {
        this.id = id;
        this.title = title;
        this.topic = topic;
        this.bmp = bmp;
    }

    public String getId() { return id; }

    public String getTitle() {
        return title;
    }

    public String getTopic() {
        return topic;
    }

    public Bitmap getIcon() {
        return bmp;
    }
}
