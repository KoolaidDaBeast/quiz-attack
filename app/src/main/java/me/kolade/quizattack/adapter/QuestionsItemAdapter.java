package me.kolade.quizattack.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import me.kolade.quizattack.R;
import me.kolade.quizattack.utility.ActivityUtility;

public class QuestionsItemAdapter extends RecyclerView.Adapter<QuestionsItemAdapter.QuestionViewHolder> {

    private Context ctx;
    private List<QuestionItem> itemList;
    private boolean[] savedQuestions = new boolean[10];
    private ClickListener clickListener;

    public QuestionsItemAdapter(Context ctx, List<QuestionItem> list, ClickListener cl){
        this.ctx = ctx;
        this.itemList = list;
        this.clickListener = cl;
    }

    public enum EventType {
        SELECT_PICTURE,
        SELECT_ANSWER,
        PICTURE_CLICKED,
        SAVE_QUESTION
    }

    public interface ClickListener {
        void onClick(EventType type, View v, int clickedId, int position);
    }

    public class QuestionViewHolder extends RecyclerView.ViewHolder {

        //Hold the content
        public TextView lblTitle;
        public EditText txtQuestion;
        public ImageView btnAddImage, imgContent, btnSave;

        public QuestionViewHolder(View view) {
            super(view);
            lblTitle = view.findViewById(R.id.lblHeader);
            txtQuestion = view.findViewById(R.id.txtQuestion);
            btnAddImage = view.findViewById(R.id.btnAddImage);
            imgContent = view.findViewById(R.id.imgContent);
            btnSave = view.findViewById(R.id.btnSave);
        }

    }

    @NonNull
    @Override
    public QuestionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(ctx).inflate(R.layout.row_quiz_question, parent, false);
        return new QuestionsItemAdapter.QuestionViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final QuestionViewHolder holder, final int position) {
        Resources res = holder.itemView.getResources();
        holder.lblTitle.setText("Question " + (position + 1));
        holder.txtQuestion.setText(itemList.get(position).getQuestion());
        holder.imgContent.setImageBitmap(itemList.get(position).getQuestionImage());

        holder.txtQuestion.clearFocus();

        //Show status of save button
        if (savedQuestions[position]) {
            holder.btnSave.setImageResource(R.drawable.ic_save_green_24dp);
        }
        else {
            holder.btnSave.setImageResource(R.drawable.ic_save_red_24dp);
        }


        //Set question image if is isnt null
        if (itemList.get(position).getQuestionImage() != null) {
            holder.imgContent.setVisibility(View.VISIBLE);
        }
        else {
            holder.imgContent.setVisibility(View.GONE);
        }

        //Tick the correct checked box
        for (int i = 0; i < 4; i++) {
            int id = res.getIdentifier("chkOption" + (i + 1), "id", holder.itemView.getContext().getPackageName());
            final CheckBox checkBox = holder.itemView.findViewById(id);

            if (itemList.get(position).getCorrectQuestion() == i) {
                checkBox.setChecked(true);
            }
            else {
                checkBox.setChecked(false);
            }

            //Set events for checked boxes
            final View currentView = holder.itemView;
            checkBox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    clickListener.onClick(EventType.SELECT_ANSWER, currentView, checkBox.getId(), position);
                }
            });
        }

        //Set all the options
        for (int i = 0; i < 4; i++) {
            int id = res.getIdentifier("txtOption" + (i + 1), "id", holder.itemView.getContext().getPackageName());
            EditText txtOption = holder.itemView.findViewById(id);
            txtOption.setText(itemList.get(position).getOptions()[i]);

            //Indicate to user to save question
            txtOption.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (hasFocus) {
                        savedQuestions[position] = false;
                        holder.btnSave.setImageResource(R.drawable.ic_save_red_24dp);
                    }
                }
            });
        }

        final View currentView = holder.itemView;

        //Indicate to user to save question
        holder.txtQuestion.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    savedQuestions[position] = false;
                    holder.btnSave.setImageResource(R.drawable.ic_save_red_24dp);
                }
            }
        });

        //Indicate to user to save question
        holder.btnAddImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                savedQuestions[position] = false;
                holder.btnSave.setImageResource(R.drawable.ic_save_red_24dp);
                clickListener.onClick(EventType.SELECT_PICTURE, currentView, holder.btnAddImage.getId(), position);
            }
        });

        //Indicate to user to save question
        holder.imgContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                savedQuestions[position] = false;
                holder.btnSave.setImageResource(R.drawable.ic_save_red_24dp);
                clickListener.onClick(EventType.PICTURE_CLICKED, currentView, holder.imgContent.getId(), position);
            }
        });

        //Indicate to user that question is in save state
        holder.btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                savedQuestions[position] = true;
                holder.btnSave.setImageResource(R.drawable.ic_save_green_24dp);
                clickListener.onClick(EventType.SAVE_QUESTION, currentView, holder.btnSave.getId(), position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public QuestionItem getItem(int position) {
        return itemList.get(position);
    }

    public void addItem() {
        itemList.add(new QuestionItem(getItemCount(), "", null, 1));
        notifyItemChanged(getItemCount() - 1);
    }

    public QuestionItem removeItem(int position) {
        QuestionItem item = itemList.remove(position);
        notifyDataSetChanged();
        return item;
    }

    public void setItem(int position, QuestionItem item){
        itemList.set(position, item);
        notifyItemChanged(position);
    }

    public boolean isSaved(int position) { return savedQuestions[position]; }

}

