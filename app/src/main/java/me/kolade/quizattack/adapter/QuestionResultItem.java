package me.kolade.quizattack.adapter;

import java.util.List;

public class QuestionResultItem {

    private int questionNumber;
    private boolean correct;

    public QuestionResultItem(int questionNumber, boolean correct) {
        this.questionNumber = questionNumber;
        this.correct = correct;
    }

    public int getQuestionNumber() { return questionNumber; }

    public boolean isCorrect() { return correct; }
}
