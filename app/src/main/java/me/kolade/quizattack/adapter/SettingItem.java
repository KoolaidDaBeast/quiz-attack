package me.kolade.quizattack.adapter;

import android.graphics.Bitmap;

public class SettingItem {

    private int id, img;
    private boolean checked = false;
    private String desc, inputText = "";
    private Type type;
    private Bitmap bmp = null;

    public SettingItem(int img, String desc, Type type) {
        this.img = img;
        this.type = type;
        this.desc = desc;
    }

    public SettingItem(String desc, Type type) {
        this.type = type;
        this.desc = desc;
    }

    public int getImage() {
        return img;
    }

    public String getDesc() {
        return desc;
    }

    public Type getType() {
        return type;
    }

    public String getPrefillText() { return inputText; }

    public SettingItem checked(boolean checked) {
        this.checked = checked;
        return this;
    }

    public SettingItem setImage(Bitmap bmp) {
        this.bmp = bmp;
        return this;
    }

    public SettingItem setText(String inputText) {
        this.inputText = inputText;
        return this;
    }

    public boolean isChecked() {
        return checked;
    }

    public Bitmap getBitmap() { return bmp; }

    public enum Type {
        SWITCH,
        TEXT_FIELD,
        NEXT,
        NONE
    }
}
