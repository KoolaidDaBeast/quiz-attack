package me.kolade.quizattack.fragment;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.fragment.app.Fragment;

import me.kolade.quizattack.R;

public class ImagePreviewFragment extends Fragment {

    private View view;
    private Bitmap bmp;

    public ImagePreviewFragment(Bitmap bmp) {
        this.bmp = bmp;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.fragment_image_preview, null);

        //Display image
        ImageView img = view.findViewById(R.id.imgTranslate);
        img.setImageBitmap(bmp);

        return this.view;
    }
}
