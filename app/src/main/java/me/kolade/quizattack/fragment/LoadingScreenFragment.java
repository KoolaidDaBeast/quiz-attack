package me.kolade.quizattack.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import me.kolade.quizattack.R;

public class LoadingScreenFragment extends Fragment {

    private View view;

    public LoadingScreenFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.fragment_loading_screen, null);
        //return inflater.inflate(R.layout.fragment_nine_news, container, false);

        return this.view;
    }
}
